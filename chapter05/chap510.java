package chapter05;

public class chap510 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char[] abcCode = { '`','~','!','@','#','$','%','^','&','*', 
				'(',')','-','_','+','=','|','[',']','{', 
				'}',';',':',',','.','/'}; 
		char[] numCode = {'q','w','e','r','t','y','u','i','o','p'}; 
						// 0 1 2 3 4 5 6 7 8 9 
		
		String src = "abc123"; 
		StringBuffer result = new StringBuffer(); 
		//문자열을 붙이기 위해 append 활용
		//StringBuffer에 대해 알아두기
		
		//문자열 src의 문자를 charAt()으로 하나씩 읽어서 변환 후 result에 저장 
		for(int i = 0; i < src.length(); i++) { 
			char ch = src.charAt(i);
			/* 알맞은 코드를 넣어 완성하시오 (1) . */
			if(ch > 47 && ch < 57) {
				result.append(numCode[ch-48]);
			} else {
				result.append(abcCode[ch-97]);
			}
		} 
		
		System.out.println("src:"+src); 
		System.out.println("result:"+result); 
	}

}
