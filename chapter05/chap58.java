package chapter05;

public class chap58 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] answer = { 1,4,4,3,1,4,4,2,1,3,2 }; 
		int[] counter = new int[4];
		
		for(int i = 0; i < answer.length; i++) { 
			/*알맞은 코드를 넣어 완성하시오  (1) . */
			counter[answer[i]-1]++; 
			//answer 배열 안의 값이 곧 counter 배열의 index로 쓰이면서 counter의 각 index의 value를 증가시킴. 
			//-1은 배열의 인덱스가 0부터 시작하기 때문.
		} 
		
		for(int i = 0; i < counter.length; i++) { 
			/* 알맞은 코드를 넣어 완성하시오 (2) . */ 
			System.out.print(counter[i]);
			for(int j = 0; j < counter[i]; j++) {
				System.out.print('*');
			}
			System.out.println();
		} 
	}
}
